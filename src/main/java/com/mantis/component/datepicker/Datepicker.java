
package com.mantis.component.datepicker;

import javax.faces.component.FacesComponent;
import javax.faces.component.html.HtmlInputText;

@FacesComponent(createTag = true, value = Datepicker.COMPONENT_TYPE, tagName = "datePicker", namespace = "http://mantis.com.tr/components")
public class Datepicker extends HtmlInputText {

    static final String COMPONENT_TYPE = "com.mantis.component.Datepicker";
    static final String COMPONENT_FAMILY = "com.mantis.component";
    static final String DEFAULT_RENDERER = "com.mantis.component.DatepickerRenderer";

    @Override
    public String getFamily() {
        return COMPONENT_FAMILY;
    }

    @Override
    public String getRendererType() {
        return DEFAULT_RENDERER;
    }


}
